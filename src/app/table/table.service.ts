import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  baseUrl: string;

  constructor(public http: HttpClient) {
    this.baseUrl = environment.baseUrl
  }

  getList(): Observable<any> {
    const url = `https://publishing-house-service.herokuapp.com/api/v1/books/`
    // const url = this.baseUrl
    return this.http.get<any>(url)
  }
  add(params: any): Observable<any> {
    const url = `https://publishing-house-service.herokuapp.com/api/v1/books/`
    return this.http.post<any>(url, params)
  }

  update(params: any): Observable<any> {
    const url = `https://publishing-house-service.herokuapp.com/api/v1/books/${params.id}`
    return this.http.put<any>(url, params)
  }
  delete(params: any): Observable<any> {
    const url = `https://publishing-house-service.herokuapp.com/api/v1/books/${params.id}`
    return this.http.delete<any>(url, params)
  }


}
