import { TableService } from './table.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  bookList: any;
  dataStatus = 'fetching'
  p: number = 1
  cityForm: FormGroup
  selectedIndex: number = -1
  modalRef!: BsModalRef;
  modalTitle = ''
  authors: any = []
  loaderOptions = {
    rows: 5,
    cols: 9,
    colSpans: {
      0: 1
    }
  }
  constructor(private ds: TableService, private ms: BsModalService,
    private fb: FormBuilder,) {

    this.cityForm = this.fb.group({
      id: new FormControl(null),
      name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      year: new FormControl(null, [Validators.required]),
      authors: new FormControl(null, [Validators.required]),
      summary: new FormControl(null, [Validators.required]),

    })
  }

  ngOnInit() {

    this.ds.getList().subscribe((resp: any) => {

      this.bookList = resp || []

    })
  }
  get g() {
    return this.cityForm.controls
  }

  openModal(modal: TemplateRef<any>, index: number) {
    this.selectedIndex = index
    this.modalTitle = 'Add Books'
    if (index > -1) {
      this.cityForm.controls['id'].setValue(this.bookList[index].id)
      this.cityForm.patchValue(this.bookList[this.selectedIndex])
      this.cityForm.controls['authors'].setValue(this.bookList[index].authors.join(','))
      this.modalTitle = 'Edit Books'
    }

    this.modalRef = this.ms.show(modal, {
      class: 'modal-lg modal-dialog-centered back-office-panel',
      backdrop: 'static',
      ignoreBackdropClick: true,
      keyboard: false
    })
  }
  confirmDelModal(template: TemplateRef<any>, i: number) {
    this.selectedIndex = i
    this.modalRef = this.ms.show(template, {
      class: 'modal-sm modal-dialog-centered back-office-panel'
    })
  }
  cancelRegionButton(f: any) {
    this.modalRef.hide()
    f.resetForm()
  }
  delete() {
    const params = {
      id: this.bookList[this.selectedIndex].id
    }
    this.ds.delete(params).subscribe((resp: any): any => {
      if (resp.success === false) {
        this.modalRef.hide()
        return false
      } else {

        this.bookList.splice(this.selectedIndex, 1)
        this.modalRef.hide()
        this.selectedIndex = -1
      }
    })
  }
  save(f: any): any {
    if (this.cityForm.invalid) {

      return false
    }

    let authors = this.cityForm.value.authors.split(',')
    const params = {
      id: this.cityForm.value.id,
      name: this.cityForm.value.name,
      year: this.cityForm.value.year,
      authors: authors,
      summary: this.cityForm.value.summary

    }

    let saveUpdate = this.ds.add(params)
    if (this.cityForm.value.id !== null) {
      saveUpdate = this.ds.update(params)
    }
    saveUpdate.subscribe((resp: any): any => {
      if (resp.success === false) {
        return false
      }
      else {
        if (this.cityForm.value.id !== null) {
          this.bookList[this.selectedIndex] = params
        }
        else {
          this.bookList.unshift(params)
        }
      }

      f.resetForm()
      this.modalRef.hide()
    })
  }

}
