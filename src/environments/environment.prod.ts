export const environment = {
  production: true,
  baseUrl: 'https://publishing-house-service.herokuapp.com/api/v1/books/',
};
